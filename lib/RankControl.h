/**
 * @file RankControl.h
 * @brief Ranks can be paused or some of their data may be corrupted which is implemented by the functions declared in this file.
 * This works by sending signals to the application processes which are caught in respective handlers.
 * SIGUSR1 is the signal used to pause a rank.
 * SIGUSR2 is the signal used to corrupt data on the next heartbeat. 
 *  Created on: 2 Jul 2018
 * @author: Ben Hazelwood, Philipp Samfass
 */

#ifndef RANKCONTROL_H_
#define RANKCONTROL_H_

/**
 *   USR1 is used to pause a rank for 1s
 *   USR2 is used to corrupt the data on next heartbeat
 */
void registerSignalHandler();

/**
 * Signal handler for USR1.
 */
void pauseThisRankSignalHandler(int signum);

/**
 * Signal handler for USR2.
 */
void corruptThisRankSignalHandler(int signum);

/**
 * Returns true if data should be corrupted.
*/
bool getShouldCorruptData();

/**
 * Disables/enables corruption in next heartbeat.
 * @param toggle If true, corruption is triggered.
 */
void setShouldCorruptData(bool toggle);

#endif
