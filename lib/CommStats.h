/**
 * @file CommStats.h
 * @author Philipp Samfass
 * @brief Communication statistics track the communication volume and 
 * can output how many bytes were sent/received (currently only for p2p communication).
 */

#ifndef STATISTICS_H_
#define STATISTICS_H_

#include <mpi.h>
#include <atomic>

/**
 * Communication statistics track the communication volume and 
 * can output how many bytes were sent/received.
*/
namespace CommunicationStatistics {

 /**
  * Struct for storing the communication statistics
  */
 struct CommunicationStats {
    /**
     *  number of sent bytes
     */
    std::atomic<size_t> sentBytes; 
    /**
     *  number of received bytes
     */
    std::atomic<size_t> receivedBytes; 
 }; 
 /**
  * Computes the communication volume in bytes for a message of give datatype and count.
  * @param datatype The MPI datatype being sent/received.
  * @param count The number of datatype elements.
  */
 size_t computeCommunicationVolume(MPI_Datatype datatype, int count);

 /**
  * Tracks a send operation in the statistics.
  * @param datatype The MPI datatype being sent.
  * @param count The number of datatype elements.
  */
 void trackSend(MPI_Datatype datatype, int count);

 /**
  * Tracks a receive operation in the statistics.
  * @param datatype The MPI datatype being received.
  * @param count The number of datatype elements.
  */
 void trackReceive(MPI_Datatype datatype, int count); 

 /**
  * Dumps the communication statistics into a file.
  * The output path and filename prefix are read from environment variables
  * TMPI_STATS_OUTPUT_PATH and TMPI_STATS_FILENAME_PREFIX.
  * TeaMPI automatically appends team number and rank to the filename prefix.
  */
 void outputCommunicationStatistics();

}

#endif /* STATISTICS_H_ */

