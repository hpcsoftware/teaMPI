/**
 * @file Timing.h
 * @brief Manages heartbeats (sending and receiving) and contains functionality for dumping heartbeats post-mortem to an output file. 
 * @author Ben Hazelwood, Philipp Samfass
 */

#ifndef TIMING_H_
#define TIMING_H_

#include <mpi.h>

/**
 * Contains routines for managing heartbeats.
 */
namespace Timing {

/**
 * Tracks start and end of a heartbeat and stores time between heartbeats.
 * @param tag Tags for this heartbeat. A positive tag x starts the heartbeat, the corresponding negative tag -x ends the heartbeat..
 */
void markTimeline(int tag);
/**
 * Tracks start and end of a heartbeat, stores time between heartbeats and also keeps track of hashes over send buffers (for corruption detection).
 * @see markTimeline
 * @param tag Tag for this heartbeat.
 * @param sendbuf Send buffer that is hashed.
 * @param sendcount Length of sendbuffer in MPI_Datatype sendtype
 * @param sendtype MPI datatype contained in sendbuffer
 */
void markTimeline(int tag, const void *sendbuf, int sendcount, MPI_Datatype sendtype);

/**
 * Initialises data structures for heartbeats.
 */
void initialiseTiming();

/**
 * Destroys data structures for heartbeats.
 */
void finaliseTiming();

/**
 *  Compare the time of heartbeat(s) with other replica(s)
 */
void compareProgressWithReplicas();
/**
 *  Also compare a hash of a heartbeat buffer.
 */
void compareBufferWithReplicas(const void *sendbuf, int sendcount, MPI_Datatype sendtype);

/**
 * Probes for a heartbeat from the replica of a given team.
 * @param targetTeam Team of the replica from which the heartbeat should be received.
 */
void pollForAndReceiveHeartbeat(int targetTeam);

/**
 * Makes progress on outstanding communication requests for the given team.
 * @param targetTeam Team for which progress on outstanding requests should be made.
 */
void progressOutstandingRequests(int targetTeam);

/**
 * Tracks points in time when sleep is invoked.
*/
void sleepRankRaised();

/**
 * Dumps heartbeat statistics
 */
void outputTiming();

}
#endif /* TIMING_H_ */
