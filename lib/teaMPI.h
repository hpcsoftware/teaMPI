/**
 * @file teaMPI.h
 * @mainpage TeaMPI
 * Welcome to the source code documentation of teaMPI.
 * TeaMPI is a PMPI wrapper library to achieve transparent replication of MPI processes.
 * Please find more documentation on building, installing and running with teaMPI in the guidebook in the folder documentation.
 * @image html ./teaMPI.jpg 
 */

#ifndef _TEAMPI_H_
#define _TEAMPI_H_

#include <mpi.h>

/**
 * Returns an MPI communicator for inter-team replication. 
 * Currently, only communication between replica ranks is allowed.
 * E.g., team 0 rank x can only communicate with team 1 rank x and team 2 rank x.
 * A rank in the returned MPI communicator refers to the team number. 
 * E.g., if we send something to rank 2 on the communicator, the data is transferred
 * to the replica rank in team 2.
*/
MPI_Comm TMPI_GetInterTeamComm();

/**
 * Returns the number of the team the calling rank belongs to.
*/
int TMPI_GetTeamNumber();

/**
 * Returns the world rank in MPI_COMM_WORLD of the calling rank.
*/
int TMPI_GetWorldRank();

/**
 * Returns the size of the inter-team communicator which is equal to the number of teams (but may be changed in the future if required).
*/
int TMPI_GetInterTeamCommSize();

/**
 * Returns 1 if the calling rank belongs to team 0.
 * This routine is useful if only a single team should execute some actions such as file I/O.
*/
int TMPI_IsLeadingRank();

#endif
