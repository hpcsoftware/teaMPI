/*
 * RankOperations.cpp
 *
 *  Created on: 2 Mar 2018
 *      Author: Ben Hazelwood
 */

#include "Rank.h"

#include <cassert>
#include <cstdlib>
#include <iostream>

#include "RankControl.h"
#include "Logging.h"
#include "Timing.h"

#ifdef USE_SMARTMPI
#include "mpi_offloading.h"
#endif

static int worldRank;
static int worldSize;
static int teamRank;
static int teamSize;
static int numTeams;

static bool assignConsecutiveRanksRR = false;

static MPI_Comm TMPI_COMM_TEAM;
MPI_Comm TMPI_COMM_DUP;
static MPI_Comm TMPI_COMM_INTER_TEAM;

int initialiseTMPI( MPI_Comm comm) {
  /**
   * The application should have no knowledge of the world_size or world_rank
   */
  setEnvironment();

  PMPI_Comm_size(comm, &worldSize);
  PMPI_Comm_rank(comm, &worldRank);
  teamSize = worldSize / numTeams;

  int color;
  if(assignConsecutiveRanksRR) {
    color = worldRank % numTeams;
  }
  else {
    color = worldRank / teamSize;
  }

  PMPI_Comm_dup(comm, &TMPI_COMM_DUP);

  PMPI_Comm_split(comm, color, worldRank, &TMPI_COMM_TEAM);
#ifdef USE_SMARTMPI
  register_new_comm(TMPI_COMM_TEAM);
#endif

  PMPI_Comm_rank(TMPI_COMM_TEAM, &teamRank);

  PMPI_Comm_size(TMPI_COMM_TEAM, &teamSize);

  // Todo: free 
  PMPI_Comm_split(comm, teamRank, worldRank, &TMPI_COMM_INTER_TEAM);
#ifdef USE_SMARTMPI
  register_new_comm(TMPI_COMM_INTER_TEAM);
#endif

  assert(teamSize == (worldSize / numTeams));

  registerSignalHandler();
  outputEnvironment();

#ifndef REPLICAS_OUTPUT
  // Disable output for all but master replica (0)
  if (getTeam() > 0) {
//    disableLogging();
  }
#endif

  Timing::initialiseTiming();

  synchroniseRanksGlobally();

  return MPI_SUCCESS;
}


int getWorldRank() {
  return worldRank;
}

int getWorldSize() {
  return worldSize;
}

int getTeamRank() {
  return teamRank;
}

int getTeamSize() {
  return teamSize;
}

int getTeam() {
  if(assignConsecutiveRanksRR) {
    return getWorldRank() % getTeamSize();
  }
  else {
    return getWorldRank() / getTeamSize();
  }
}

int getNumberOfTeams() {
  return getWorldSize() / getTeamSize();
}

MPI_Comm getTeamComm(MPI_Comm comm) {
  return (comm==MPI_COMM_WORLD) ? TMPI_COMM_TEAM : comm;
}

int freeTeamComm() {
  return MPI_Comm_free(&TMPI_COMM_TEAM);
}

MPI_Comm getTeamInterComm() {
  return TMPI_COMM_INTER_TEAM;
}

int freeTeamInterComm() {
  return MPI_Comm_free(&TMPI_COMM_INTER_TEAM);
}

MPI_Comm getLibComm() {
  return TMPI_COMM_DUP;
}

int freeLibComm() {
  return MPI_Comm_free(&TMPI_COMM_DUP);
}

std::string getEnvString(std::string const& key)
{
    char const* val = std::getenv(key.c_str());
    return val == nullptr ? std::string() : std::string(val);
}

void outputEnvironment(){
  if(worldSize % numTeams != 0) {
    std::cerr <<"Wrong choice of world size and number of teams!\n";
    std::cerr <<"Number of teams: " << numTeams << "\n";
    std::cerr <<"Total ranks: " << worldSize << "\n"; 
  }

  assert(worldSize % numTeams == 0);

  PMPI_Barrier(TMPI_COMM_DUP);
  double my_time = MPI_Wtime();
  PMPI_Barrier(TMPI_COMM_DUP);
  double *times = new double[worldSize];

  PMPI_Gather(&my_time, 1, MPI_DOUBLE, times, 1, MPI_DOUBLE, MASTER, TMPI_COMM_DUP);

  PMPI_Barrier(TMPI_COMM_DUP);

  if (worldRank == MASTER) {
    std::cout << "------------TMPI SETTINGS------------\n";
    std::cout << "Number of teams: " << numTeams << "\n";

    std::cout << "Team size: " << teamSize << "\n";
    std::cout << "Total ranks: " << worldSize << "\n\n";


    if (worldRank == MASTER) {
      for (int i=0; i < worldSize; i++) {
        std::cout << "Tshift(" << i << "->" << mapWorldToTeamRank(i) << "->" << mapTeamToWorldRank(mapWorldToTeamRank(i),mapRankToTeamNumber(i)) << ") = " << times[i] - times[0] << "\n";
      }
    }
    std::cout << "---------------------------------------\n\n";
  }

  delete[] times;
  PMPI_Barrier(TMPI_COMM_DUP);
}

void setEnvironment() {
  std::string env(getEnvString("TEAMS"));
  numTeams = env.empty() ? 2 : std::stoi(env);

  std::string env_rr(getEnvString("ASSIGN_RR"));
  assignConsecutiveRanksRR = env_rr.empty() ? false : true;
}

int mapRankToTeamNumber(int rank) {
  if(assignConsecutiveRanksRR) {
    return rank % getTeamSize();
  }
  else {
    return rank / getTeamSize();
  }
}

int mapWorldToTeamRank(int rank) {
  if (rank == MPI_ANY_SOURCE) {
    return MPI_ANY_SOURCE;
  } else {
    if(assignConsecutiveRanksRR) {
      return rank / getNumberOfTeams();
    }
    else {
      return rank % getTeamSize();
    }
  }
}

int mapTeamToWorldRank(int rank, int team) {
  if (rank == MPI_ANY_SOURCE) {
    return MPI_ANY_SOURCE;
  }

  if(assignConsecutiveRanksRR) {
    return rank * getNumberOfTeams() + team;
  }
  else {
    return rank + team * getTeamSize();
  }
}

int translateRank(MPI_Comm srcComm, int srcRank, MPI_Comm destComm) {
  MPI_Group srcCommGroup, destCommGroup;
  int result;  

  MPI_Comm_group(srcComm, &srcCommGroup);
  MPI_Comm_group(destComm, &destCommGroup);
 
  MPI_Group_translate_ranks(srcCommGroup, 1, &srcRank, destCommGroup, &result);
  return result;
} 

int mapToWorldRank(int rank, MPI_Comm comm) {
  return translateRank(comm, rank, MPI_COMM_WORLD); 
}

void remapStatus(MPI_Status *status) {
  if ((status != MPI_STATUS_IGNORE ) && (status != MPI_STATUSES_IGNORE )) {
    status->MPI_SOURCE = mapWorldToTeamRank(status->MPI_SOURCE);
    logInfo(
        "remap status source " << status->MPI_SOURCE << " to " << mapWorldToTeamRank(status->MPI_SOURCE));
  }
}

int synchroniseRanksInTeam() {
  return PMPI_Barrier(getTeamComm(MPI_COMM_WORLD));
}

int synchroniseRanksGlobally() {
  return PMPI_Barrier(getLibComm());
}


