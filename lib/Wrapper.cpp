#include "Wrapper.h"

#include <cassert>
#include <unistd.h>

#include "Logging.h"
#include "Rank.h"
#include "Timing.h"
#include "CommStats.h"


#ifdef USE_SMARTMPI
#include "mpi_offloading.h"
#include "mpi_offloading_common.h"
#endif


int MPI_Init(int *argc, char*** argv) {
  int err = PMPI_Init(argc, argv);
#ifdef USE_SMARTMPI
  smpi_init( (*argv)[0]);
  if(!_is_server)
    initialiseTMPI(_comm);
#else
  initialiseTMPI(MPI_COMM_WORLD);
#endif
  return err;
}

int MPI_Init_thread( int *argc, char ***argv, int required, int *provided ) {
  int err = PMPI_Init_thread(argc, argv, required, provided);
#ifdef USE_SMARTMPI
  smpi_init( (*argv)[0]);
  if(!_is_server)
    initialiseTMPI(_comm);
#else
  initialiseTMPI(MPI_COMM_WORLD);
#endif
  return err;
}

int MPI_Is_thread_main(int* flag) {
  // See header documentation
  *flag = getTeam();
  return MPI_SUCCESS;
}

int MPI_Comm_split(MPI_Comm comm, int color, int key, MPI_Comm *newcomm) {
  if(comm==MPI_COMM_WORLD)
#ifdef USE_SMARTMPI
    PMPI_Comm_split(_comm, color, key, newcomm);
#ifdef USE_SMARTMPI
    register_new_comm(*newcomm);
#endif
#else
    PMPI_Comm_split(comm, color, key, newcomm);
#endif
  return MPI_SUCCESS; 
}

int MPI_Comm_rank(MPI_Comm comm, int *rank) {
  // todo: assert that a team comm is used
  //assert(comm == MPI_COMM_WORLD);  

  if(comm==MPI_COMM_WORLD) 
#ifdef USE_SMARTMPI
    if(!_is_server) 
     *rank = getTeamRank();
    else {
     PMPI_Comm_rank(_comm, rank);
    }
#else
     *rank = getTeamRank();
#endif
  else 
   PMPI_Comm_rank(comm, rank);
  return MPI_SUCCESS;
}

int MPI_Comm_size(MPI_Comm comm, int *size) {
  //assert(comm == MPI_COMM_WORLD);
  *size = getTeamSize();
  return MPI_SUCCESS;
}

int MPI_Comm_dup(MPI_Comm comm, MPI_Comm *newcomm) {
  int err = PMPI_Comm_dup(getTeamComm(comm), newcomm);
  logInfo("Created communicator " << *newcomm);
  assert(err==MPI_SUCCESS);
#ifdef USE_SMARTMPI
  register_new_comm(*newcomm);
#endif
  return err;
}

int MPI_Comm_free(MPI_Comm *comm) {
  assert(*comm != MPI_COMM_WORLD);
  logInfo("Free communicator " << *comm);
  int err = PMPI_Comm_free(comm);
  assert(err==MPI_SUCCESS);
  return err;
}

int MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest,
              int tag, MPI_Comm comm) {
#ifdef COMM_STATS
  CommunicationStatistics::trackSend(datatype,  count);
#endif
  //assert(comm == MPI_COMM_WORLD);
  int err;
#ifdef USE_SMARTMPI
  if(_is_server) {
    MPI_Comm mapped_comm = (comm==MPI_COMM_WORLD) ? _comm : comm;
    err = PMPI_Send(buf, count, datatype, dest, tag, mapped_comm);
  }
  else {
    err = PMPI_Send(buf, count, datatype, dest, tag, getTeamComm(comm));  
  }
#else
  err = PMPI_Send(buf, count, datatype, dest, tag, getTeamComm(comm));  
#endif
  logInfo("Send to rank " << dest << "/" << mapTeamToWorldRank(dest) << " with tag " << tag);
  return err;
}

int MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source, int tag,
              MPI_Comm comm, MPI_Status *status) {
  //assert(comm == MPI_COMM_WORLD);
  int err;
#ifdef USE_SMARTMPI 
  if(_is_server) {
    MPI_Comm mapped_comm = (comm==MPI_COMM_WORLD) ? _comm : comm;
    err = PMPI_Recv(buf, count, datatype, source, tag, mapped_comm, status);
  }
  else {
    err = PMPI_Recv(buf, count, datatype, source, tag, getTeamComm(comm), status);     
  }
#else
#ifdef COMM_STATS
  CommunicationStatistics::trackReceive(datatype,  count);
#endif
   err = PMPI_Recv(buf, count, datatype, source, tag, getTeamComm(comm), status);
#endif
  logInfo("Receive from rank " << source << "/" << mapTeamToWorldRank(source) << " with tag " << tag);
  return err;
}

int MPI_Allgather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
                  void *recvbuf, int recvcount, MPI_Datatype recvtype,
                  MPI_Comm comm) {
  int err = PMPI_Allgather(sendbuf, sendcount, sendtype,
                           recvbuf, recvcount, recvtype,
                           getTeamComm(comm)); 
  return err;
}

int MPI_Iallgather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
                   void *recvbuf, int recvcount, MPI_Datatype recvtype,
                   MPI_Comm comm, MPI_Request * request) {
  int err = PMPI_Iallgather(sendbuf, sendcount, sendtype,
                            recvbuf, recvcount, recvtype,
                            getTeamComm(comm), request);
  return err;
}

int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest,
               int tag, MPI_Comm comm, MPI_Request *request) {
  //assert(comm == MPI_COMM_WORLD);
  int err;
#ifdef USE_SMARTMPI
  if(_is_server) {
    MPI_Comm mapped_comm = (comm==MPI_COMM_WORLD) ? _comm : comm;
    err = PMPI_Isend(buf, count, datatype, dest, tag, mapped_comm, request);
  }
  else {
    err = PMPI_Isend(buf, count, datatype, dest, tag, getTeamComm(comm), request);  
  }
#else
#ifdef COMM_STATS
  CommunicationStatistics::trackSend(datatype,  count);
#endif
  err = PMPI_Isend(buf, count, datatype, dest, tag, getTeamComm(comm), request);
  logInfo("Isend to rank " << dest << "/" << mapTeamToWorldRank(dest) << " with tag " << tag);
#endif 
  return err;
}

int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source, int tag,
               MPI_Comm comm, MPI_Request *request) {
  //assert(comm == MPI_COMM_WORLD);
#ifdef COMM_STATS
  CommunicationStatistics::trackReceive(datatype,  count);
#endif
  int err = PMPI_Irecv(buf, count, datatype, source, tag, getTeamComm(comm), request);
  logInfo("Receive from rank " << source << "/" << mapTeamToWorldRank(source) << " with tag " << tag);
  return err;
}

int MPI_Wait(MPI_Request *request, MPI_Status *status) {
  logInfo("Wait initialised");
  int err = PMPI_Wait(request, status);
  logInfo("Wait completed");
  return err;
}

int MPI_Waitall(int count, MPI_Request array_of_requests[], MPI_Status array_of_statuses[]) {
  logInfo("Waitall initialised with " << count << " requests");
  int err = PMPI_Waitall(count, array_of_requests, array_of_statuses);
  logInfo("Waitall completed with " << count << " requests");
  return err;
}

int MPI_Test(MPI_Request *request, int *flag, MPI_Status *status) {
  int err = PMPI_Test(request, flag, status);
  logInfo("Test completed (FLAG=" << *flag << ",STATUS_SOURCE=" << status->MPI_SOURCE << ",STATUS_TAG=" << status->MPI_TAG << ")");
  return err;
}

int MPI_Probe(int source, int tag, MPI_Comm comm, MPI_Status *status) {
  //assert(comm == MPI_COMM_WORLD);
  logInfo("Probe initialised (SOURCE=" << source << ",TAG=" << tag << ")");
  int err = PMPI_Probe(source, tag, getTeamComm(comm), status);
  logInfo("Probe finished (SOURCE=" << mapTeamToWorldRank(source) << ",TAG=" << tag << ",STATUS_SOURCE=" << status->MPI_SOURCE << ",STATUS_TAG=" << status->MPI_TAG << ")");
  return err;
}

int MPI_Iprobe(int source, int tag, MPI_Comm comm, int *flag,
                MPI_Status *status) {
  //assert(comm == MPI_COMM_WORLD);
  int err;
#ifdef USE_SMARTMPI
  if(_is_server) {
    if(comm==MPI_COMM_WORLD)
      err = PMPI_Iprobe(source, tag, _comm, flag, status);
    else 
      err = PMPI_Iprobe(source, tag, comm, flag, status);      
  }
  else {
    err = PMPI_Iprobe(source, tag, getTeamComm(comm), flag, status);
  }
#else  
    err = PMPI_Iprobe(source, tag, getTeamComm(comm), flag, status);
#endif
  logInfo("Iprobe finished (FLAG=" << *flag << ",SOURCE=" << mapTeamToWorldRank(source) << ",TAG=" << tag << ",STATUS_SOURCE=" << status->MPI_SOURCE << ",STATUS_TAG=" << status->MPI_TAG << ")");
  return err;
}

#ifdef USE_SMARTMPI

int MPI_Iprobe_offload(int source, int tag, MPI_Comm comm, int *flag, MPI_Status_Offload *status) {
  int ierr;
  assert(source==MPI_ANY_SOURCE);
#if COMMUNICATION_MODE==0
  ierr = smpi_iprobe_offload_p2p(source, tag, comm, flag, status);
#elif COMMUNICATION_MODE==2
  //ierr = smpi_iprobe_offload_rma(source, tag, comm, flag, status);
  ierr = smpi_iprobe_offload_p2p(source, tag, comm, flag, status);
#endif
  if(*flag)
    status->MPI_SOURCE = translateRank(MPI_COMM_WORLD, status->MPI_SOURCE, comm);
  return ierr;
}

int MPI_Send_offload(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, int rail) {

#if COMMUNICATION_MODE==0
  return smpi_send_offload_p2p(buf, count, datatype, translateRank(comm, dest, _comm), tag, comm, rail);
#elif COMMUNICATION_MODE==2
  return smpi_send_offload_gpi(buf, count, datatype, translateRank(comm, dest, _comm), tag, comm, rail);
#endif
}

int MPI_Recv_offload(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Status_Offload *stat, int rail) {
#if COMMUNICATION_MODE==0
  int ierr = smpi_recv_offload_p2p(buf, count, datatype, translateRank(comm, source, _comm), tag, comm, stat, rail);
  return ierr;
#elif COMMUNICATION_MODE==2
  //return smpi_recv_offload_rma(buf, count, datatype, translateRank(comm, source, _comm), tag, comm, stat);
  return smpi_recv_offload_gpi(buf, count, datatype, translateRank(comm, source, _comm), tag, comm, stat, rail);
#endif
}

#endif

int MPI_Barrier(MPI_Comm comm) {
  //assert(comm == MPI_COMM_WORLD);
  int err = synchroniseRanksInTeam();
  return err;
}

int MPI_Bcast( void *buffer, int count, MPI_Datatype datatype, int root,
               MPI_Comm comm ) {
  //assert(comm == MPI_COMM_WORLD);
  int err = PMPI_Bcast(buffer, count, datatype, root, getTeamComm(comm));
  return err;
}

int MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
                  MPI_Datatype datatype, MPI_Op op, MPI_Comm comm) {
  //assert(comm == MPI_COMM_WORLD);
  int err = PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, getTeamComm(comm));
  return err;
}

int MPI_Alltoall(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
                 void *recvbuf, int recvcount, MPI_Datatype recvtype,
                 MPI_Comm comm) {
  //assert(comm == MPI_COMM_WORLD);
  int err = PMPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, getTeamComm(comm));
  return err;
}

int MPI_Alltoallv(const void *sendbuf, const int *sendcounts,
                  const int *sdispls, MPI_Datatype sendtype, void *recvbuf,
                  const int *recvcounts, const int *rdispls, MPI_Datatype recvtype,
                  MPI_Comm comm) {
  //assert(comm == MPI_COMM_WORLD);
  int err = PMPI_Alltoallv(sendbuf, sendcounts, sdispls, sendtype, recvbuf, recvcounts, rdispls, recvtype, getTeamComm(comm));
  return err;
}

double MPI_Wtime() {
  // If you mark on the timeline here expect negative time values (you've been warned)
  return PMPI_Wtime();
}

int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
                int dest, int sendtag,
                void *recvbuf, int recvcount, MPI_Datatype recvtype,
                int source, int recvtag,
                MPI_Comm comm, MPI_Status *status) {
  if (comm == MPI_COMM_SELF) {
    if (sendcount == 0) {
      Timing::markTimeline(sendtag);
    } else {
      Timing::markTimeline(sendtag, sendbuf, sendcount, sendtype);
    }
  } else {
    //assert(comm == MPI_COMM_WORLD);
    PMPI_Sendrecv(sendbuf, sendcount, sendtype,dest,sendtag,recvbuf,recvcount,recvtype,source,recvtag, getTeamComm(comm),status);
  }
  return MPI_SUCCESS;
}

int MPI_Finalize() {
#ifdef USE_SMARTMPI
  if(!_is_server) {
#endif
    logInfo("Finalize");
    Timing::finaliseTiming();
    // Wait for all replicas before finalising
    PMPI_Barrier(TMPI_COMM_DUP);
    freeTeamComm();
    Timing::outputTiming();
#ifdef USE_SMARTMPI
  }
  smpi_finalize();
#endif
#ifdef COMM_STATS
  CommunicationStatistics::outputCommunicationStatistics();
#endif
#ifdef DirtyCleanUp
  return MPI_SUCCESS;
#endif
  freeTeamInterComm();
  return PMPI_Finalize();
}

int MPI_Abort(MPI_Comm comm, int errorcode) {
  assert(comm == MPI_COMM_WORLD);
  int err = PMPI_Abort(getTeamComm(comm), errorcode);
  return err;
}
