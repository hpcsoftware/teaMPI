/**
 * @file Rank.h
 * @brief Several routines to manage ranks and communicators in teaMPI.
 * @author Ben Hazelwood, Philipp Samfass
 */

#ifndef RANK_H_
#define RANK_H_

#include <mpi.h>
#include <string>
#include <stdint.h>
#include <limits.h>

#define MASTER 0

// From https://stackoverflow.com/questions/40807833/sending-size-t-type-data-with-mpi
#if SIZE_MAX == UCHAR_MAX
   #define TMPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
   #define TMPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
   #define TMPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
   #define TMPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
   #define TMPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#else
   #error "Cannot decipher SIZE_MAX"
#endif

extern MPI_Comm TMPI_COMM_DUP;

/**
 *  Split ranks into teams.
*/
int initialiseTMPI(MPI_Comm comm);

/**
 *  Returns rank of calling process in MPI_COMM_WORLD.
 */
int getWorldRank();

/**
 * Returns size of calling process in MPI_COMM_WORLD.
 */
int getWorldSize();

/**
 *  Get the rank as seen by the application
*/
int getTeamRank();

/**
 *  Get the number of ranks as seen by the application
 */
int getTeamSize();

/** Get the number of replicas==number of teams
 *
 */
int getNumberOfTeams();

/**
 *  Return which team this rank belongs to.
 */
int getTeam();

/**
 *  Return the communicator used by this team
 */
MPI_Comm getTeamComm(MPI_Comm comm);
/**
 *  Frees the communicator used by the calling team
 */
int freeTeamComm();

/**
 *  Returns communictor for horizontal communication between replica ranks.
 */
MPI_Comm getTeamInterComm();

/**
 *  Frees inter-team communicator.
 */
int freeTeamInterComm();

/**
 *  The duplicate MPI_COMM_WORLD used by the library
 */
MPI_Comm getLibComm();

/**
 * Frees duplicate MPI_COMM_WORLD used by the library
 */
int freeLibComm();

/**
 *  Get the value of an environment variable (empty string if undefined)
 */
std::string getEnvString(std::string const& key);

/**
 * Get the number of teams from environment
 */
void setEnvironment();

/**
 * Output team sizes and any timing inaccuracies between ranks
 */
void outputEnvironment();

/**
 * Output the timing differences between replicas
 */
void outputTiming();

/**
 * Maps a world rank to its team.
 * @param rank The input rank.
 */
int mapRankToTeamNumber(int rank);

/**
 * Maps a world rank to its team rank.
 * @param rank The input rank.
 */
int mapWorldToTeamRank(int rank);

/**
 * Maps a team rank in some given team to the world rank.
 * @param rank The input rank within a team
 * @param team The team the input rank belongs to
 */
int mapTeamToWorldRank(int rank, int team);

/**
 * Translates a rank in a src communicator to the matching rank in a destination communicator.
 * @param srcComm The source communicator
 * @param srcRank The rank in the source communicator
 * @param destComm The destination communicator
 */
int translateRank(MPI_Comm srcComm, int srcRank, MPI_Comm destComm);

/**
 * Map a rank in a communicator comm to the world rank in MPI_COMM_WORLD.
 * @param rank The input rank
 * @param comm The communicator this rank number belongs to
 */
int mapToWorldRank(int rank, MPI_Comm comm);

/**
 *  Alters the MPI_SOURCE member of MPI_Status to 0 <= r < team size
 */
void remapStatus(MPI_Status *status);

/**
 *  Barrier on team communicator
 */
int synchroniseRanksInTeam();

/**
 *  Barrier on all ranks (not called by application)
 */
int synchroniseRanksGlobally();

#endif /* RANK_H_ */
